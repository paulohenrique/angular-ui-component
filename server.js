var Hapi = require('hapi');


var server = new Hapi.Server('0.0.0.0', 8080);

server.route({
  method: 'GET',
  path: '/{param*}',
  handler: {
    directory: {
      path: 'public',
      listing: true
    }
  }
});

server.start()
